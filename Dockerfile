FROM php:7.4-apache

# set a directory for the app
WORKDIR /var/www/html

# install dependencies
RUN docker-php-ext-install mysqli
RUN apt-get update && apt-get upgrade -y

# enable apache modules
RUN a2enmod rewrite

# copy all the files to the container
COPY . .