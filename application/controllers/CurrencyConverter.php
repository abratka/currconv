<?php
defined('BASEPATH') or exit('No direct script access allowed');

class CurrencyConverter extends CI_Controller
{
	private $apiUrl;
	private $apiKey;
	private $currency1;
	private $currency2;

	public function __construct()
	{
		parent::__construct();
		$config = $this->config->item('currency_converter');

		$this->apiUrl = $config['apiUrl'];
		$this->apiKey = $config['apiKey'];
		$this->currency1 = $config['currency1'];
		$this->currency2 = $config['currency2'];
	}

	public function index()
	{
		try {
			$query1 = sprintf('%s_%s', $this->currency2, $this->currency1);
			$query2 = sprintf('%s_%s', $this->currency1, $this->currency2);
			$query = implode(',', [$query1, $query2]);

			$data = [
				'exchanges' => $this->exchanges(),
				'currencies' => $this->currencies(),
				'currency1' => $this->currency1,
				'currency2' => $this->currency2,
				'amount1' => 1,
				'amount2' => 1
			];

			$rates = $this->rates($query);

			$data['amount2'] = $rates[$query2];
			$data['rate1'] = $rates[$query1];
			$data['rate2'] = $rates[$query2];

			$this->load->view('currency_converter', $data);
		} catch (Exception $e) {
			log_message('error: ', $e->getMessage());
			return null;
		}
	}

	public function currencyRate()
	{
		try {
			$post = json_decode(file_get_contents("php://input"));
			$query1 = sprintf('%s_%s', $post->currency1, $post->currency2);
			$query2 = sprintf('%s_%s', $post->currency2, $post->currency1);
			$query = implode(',', [$query1, $query2]);

			$rates = $this->rates($query);

			$this->output
				->set_content_type('application/json')
				->set_output(json_encode([
					'currency1' => $rates[$query1],
					'currency2' => $rates[$query2]
				]));
		} catch (Exception $e) {
			log_message('error: ', $e->getMessage());
			return null;
		}
	}

	private function rates($query)
	{
		try {
			$json = file_get_contents("{$this->apiUrl}/api/v7/convert?q={$query}&compact=ultra&apiKey={$this->apiKey}");
			return json_decode($json, true);
		} catch (Exception $e) {
			log_message('error: ', $e->getMessage());
			return null;
		}
	}

	private function currencies()
	{
		try {
			$json = file_get_contents("{$this->apiUrl}/api/v7/currencies?apiKey={$this->apiKey}");
			$currencies = json_decode($json, true);

			$data = [];
			foreach ($currencies['results'] as $id => $currency) {
				$data[$id] = sprintf('%s - %s', $id, $currency['currencyName']);
			}
			return $data;
		} catch (Exception $e) {
			log_message('error: ', $e->getMessage());
			return null;
		}
	}

	private function exchanges()
	{
		try {
			$json = file_get_contents("{$this->apiUrl}/api/v7/currencies?apiKey={$this->apiKey}");
			$exchanges = json_decode($json, true);
			return $exchanges['results'];
		} catch (Exception $e) {
			log_message('error: ', $e->getMessage());
			return null;
		}
	}
}
