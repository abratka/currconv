<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<h4>Valūtas kaukulātors</h4>
<hr>
<div class="row mb-3">
	<div class="col col-4 pr-0">
		<div class="form-group is-loading">
			<?php echo form_input('amount1', $amount1,
					'class="form-control" id="amount1"'); ?>
		</div>
	</div>
	<div class="col col-8">
		<div class="form-group">
			<?php echo form_dropdown('from', $currencies, $currency1,
					'class="custom-select" id="currency1" rate="' . $rate1 . '"'); ?>
		</div>
	</div>
	<div class="col col-4 pr-0">
		<div class="form-group">
			<?php echo form_input('amount2', $amount2,
					'class="form-control" id="amount2"'); ?>
		</div>
	</div>
	<div class="col col-8">
		<div class="form-group">
			<?php echo form_dropdown('to', $currencies, $currency2,
					'class="custom-select" id="currency2" rate="' . $rate2 . '"'); ?>
		</div>
	</div>
</div>
