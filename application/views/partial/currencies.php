<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<h4>Valūtu saraksts</h4>
<hr>
<div class="row">
	<div class="col">
		<table class="table table-sm table-striped">
			<?php foreach ($exchanges as $exchange):?>
			<tr>
				<td><?php echo $exchange['currencyName']; ?></td>
				<td width="1"><?php echo $exchange['id']; ?></td>
			</tr>
			<?php endforeach;?>
		</table>
	</div>
</div>
