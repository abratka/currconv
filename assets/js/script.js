jQuery(function($) {

	const amount1 = $('#amount1');
	const amount2 = $('#amount2');

	const currency1 = $('#currency1');
	const currency2 = $('#currency2');

	let rates = {}

	function init() {
		currency1.on('change', onCurrency)
		currency2.on('change', onCurrency)

		amount1.on('keyup', onAmount)
		amount2.on('keyup', onAmount)

		rates = {
			currency1: currency2.attr('rate'),
			currency2: currency1.attr('rate')
		}
		calculate(rates, 'currency2')
	}

	function onCurrency() {
		let id = this.id
		loading(true)
		axios.post('/currency-rate', {
			currency1: currency1.val(),
			currency2: currency2.val()
		})
		.then(function (response) {
			calculate(response.data, id)
		})
		.catch(function (error) {
			console.log(error);
		}).then(function () {
			loading(false)
		})
	}

	function onAmount() {
		calculate(rates,this.id)
	}

	function calculate(data, id) {
		let result = 0
		if ('currency1' === id) {
			result = data.currency2 * amount2.val()
			amount1.val(result)
		}
		if ('currency2' === id) {
			result = data.currency1 * amount1.val();
			amount2.val(result)
		}
		if ('amount1' === id) {
			result = data.currency1 * amount1.val();
			amount2.val(result)
		}
		if ('amount2' === id) {
			result = data.currency2 * amount2.val()
			amount1.val(result)
		}
		rates = data
	}

	function loading(state) {
		if (state) {
			amount1.addClass('loading')
			amount2.addClass('loading')
		} else {
			amount1.removeClass('loading')
			amount2.removeClass('loading')
		}
	}

	init()
});
